﻿namespace Servicos.PacienteServicos
{
    public interface IPacienteServico
    {
         public Task Manter(PacienteViewModel model,CancellationToken cancellationToken);
    }
}
