﻿namespace Servicos.PacienteServicos
{
    public class PacienteViewModel
    {
        public string id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
    }
}
