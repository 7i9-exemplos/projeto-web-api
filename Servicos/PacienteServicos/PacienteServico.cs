﻿using Dominio.Entidades;
using Infra.Repositorio.Geral;

namespace Servicos.PacienteServicos
{
    public class PacienteServico : IPacienteServico
    {
        private readonly IRepositorioGeral<Paciente> _Repositorio;
        public PacienteServico(IRepositorioGeral<Paciente> Repositorio)
        {
            _Repositorio = Repositorio;
        }
        public async Task Manter(PacienteViewModel view, CancellationToken cancellationToken)
        {
            var model = CarregarObjeto(view);
            await _Repositorio.AdicionarAsync(model, cancellationToken);
        }
        bool validarCampos(PacienteViewModel view)
        {
            if (view.nome == string.Empty)
                return false;
            if (view.cpf.Equals(string.Empty))
                return false;
            else return true;
        }
        Paciente CarregarObjeto(PacienteViewModel view)
        {
            if(validarCampos(view))
            {
                Paciente model = new Paciente();
                model.Id = Guid.NewGuid();
                model.Nome = view.nome;
                model.Cpf = view.cpf;
                if (!view.id.Equals(""))
                    model.Id = Guid.Parse(view.id);
                return model;
            }
            return new();           
        }
    }
}
