﻿using Ardalis.Specification;
namespace Infra.Repositorio.Geral
{
    public interface IRepositorioGeral<T> : IDisposable where T : class
    {
        Task AdicionarAsync(T entity, CancellationToken cancellationToken);
        Task AdicionarRangeAsync(List<T> entity, CancellationToken cancellationToken);
        void Alterar(T entity);
        void AlterarEstadoObjeto(T entity);
        Task<T> ObterPorIdAsync(Guid id, CancellationToken cancellationToken);
        Task<T> ObterPorIdEspecificacaoAsync(ISpecification<T> spec, CancellationToken cancellationToken);
        Task<T> ObterPorIdEspecificacaoAsync(ISpecification<T> spec);

        Task<T> ObterUltimo(ISpecification<T> spec, CancellationToken cancellationToken);
        IQueryable<T> ObterTodosQueryable();
        IQueryable<T> ObterTodosQueryableEspecificacao(ISpecification<T> spec, CancellationToken cancellationToken);
        IQueryable<T> ObterTodosQueryableEspecificacao(ISpecification<T> spec);


    }

}
