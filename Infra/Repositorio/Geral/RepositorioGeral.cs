﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Infra.ConfiguracaoDB;
using Microsoft.EntityFrameworkCore;
namespace Infra.Repositorio.Geral
{
    internal class RepositorioGeral<T> : IRepositorioGeral<T> where T : class
    {
        Contexto Context { get; }
        public RepositorioGeral(Contexto context)
        {
            Context = context;
        }
        public async Task AdicionarAsync(T entity, CancellationToken cancellationToken)
        {
            await Context.Set<T>().AddAsync(entity, cancellationToken);
            Commit();
        }
        public async Task AdicionarRangeAsync(List<T> entity, CancellationToken cancellationToken)
        {
            await Context.Set<T>().AddRangeAsync(entity, cancellationToken);
            Commit();
        }
        public void Alterar(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.Set<T>().Update(entity);
            Commit();
        }
        public void AlterarEstadoObjeto(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.Set<T>().Update(entity);

        }

        public async Task<T> ObterPorIdAsync(Guid id, CancellationToken cancellationToken)
        {
            return await Context.Set<T>().FindAsync(id);
        }
        public async Task<T> ObterPorIdEspecificacaoAsync(ISpecification<T> spec, CancellationToken cancellationToken)
        {
            IQueryable<T> specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstOrDefaultAsync(cancellationToken);
        }
        public async Task<T> ObterPorIdEspecificacaoAsync(ISpecification<T> spec)
        {
            IQueryable<T> specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstOrDefaultAsync();
        }

        public IQueryable<T> ObterTodosQueryable()
        {
            return Context.Set<T>().AsQueryable();
        }
        public async Task<T> ObterUltimo(ISpecification<T> spec, CancellationToken cancellationToken)
        {
            IQueryable<T> specificationResult = ApplySpecification(spec);
            return await specificationResult.FirstOrDefaultAsync(cancellationToken);
        }
        public IQueryable<T> ObterTodosQueryableEspecificacao(ISpecification<T> spec, CancellationToken cancellationToken)
        {
            IQueryable<T> specificationResult = ApplySpecification(spec);
            return specificationResult.AsQueryable();
        }
        public IQueryable<T> ObterTodosQueryableEspecificacao(ISpecification<T> spec)
        {
            IQueryable<T> specificationResult = ApplySpecification(spec);
            return specificationResult.AsQueryable();
        }

        public void Commit()
        {
            Context.SaveChanges();
        }
        public void Dispose()
        {
            Context.Dispose();
        }
        private IQueryable<T> ApplySpecification(ISpecification<T> spec)
        {
            return SpecificationEvaluator.Default.GetQuery(Context.Set<T>().AsQueryable(), spec);
        }

    }
}
