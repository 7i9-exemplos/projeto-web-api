﻿using Infra.Repositorio.Geral;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
namespace Infra.ConfiguracaoDB
{
    public static class AddBancoDeDados
    {
        public static IServiceCollection AddBancoDeDadosEstrutura(this IServiceCollection serviceCollection,
              IConfiguration configuration)
        {

            serviceCollection.AddDbContext<Contexto>(options =>
            {
                options.UseSqlServer(
                    configuration.GetConnectionString("7i9Exemplo"),
                    b => b.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery)
                        .MigrationsAssembly(typeof(Contexto).Assembly.FullName)
                );
            });
            serviceCollection.AddScoped(typeof(IRepositorioGeral<>), typeof(RepositorioGeral<>));
            return serviceCollection;
        }
    }
}
