﻿using Dominio.Entidades;
using Infra.Mapeamento;
using Microsoft.EntityFrameworkCore;


namespace Infra.ConfiguracaoDB
{
    public partial class Contexto : DbContext
    {
        private static readonly DbContextOptions options = new DbContextOptionsBuilder<Contexto>().EnableSensitiveDataLogging().Options;
        public Contexto(DbContextOptions options) : base(options)
        {

        }
        public virtual DbSet<Paciente> Backup { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PacienteConfiguracao());
            OnModelCreatingPartial(modelBuilder);
        }
        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
