﻿using Microsoft.AspNetCore.Mvc;
using Servicos.PacienteServicos;

namespace ProjetoAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PacienteController : Controller
    {
        private readonly IPacienteServico _Servico;
        public PacienteController(IPacienteServico Servico)
        {
            _Servico = Servico;
        }
        [HttpPost("Gravar")]
        public async Task<IActionResult> Post([FromBody] PacienteViewModel model, CancellationToken cancellationToken)
        {
            try
            {
                await _Servico.Manter(model, cancellationToken);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }
    }
}
